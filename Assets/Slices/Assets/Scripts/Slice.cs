﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slice : MonoBehaviour
{
    public GameObject explosionParticle;

    private bool canMove = false;
    private Vector3 targetPos;
    private Animation anim1, anim2;
    private Transform parentTransform;

    void Update()
    {
        if (canMove)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, 0.18f);        //Makes the Slice move towards the empty position
            if (Vector3.Distance(transform.position, targetPos) < 0.05f)        //If the distance between the target and the Slice is less than X, then stops movement
                StopMovement();
        }
    }

    public void SpawnParticle(bool canScore = true)
    {
        for (int i = 0; i < transform.childCount; i++)      //Loops through all of the Slices
        {
            Destroy(Instantiate(explosionParticle, transform.GetChild(i).transform.position, Quaternion.identity), 1f);     //Spawns an explosionParticle to the position of the child and destroys it after x seconds
            if (canScore)
                FindObjectOfType<ScoreManager>().IncrementScore();      //Increments score
        }
    }

    public void Move(Vector3 target, Animation a1, Animation a2, Transform transf)
    {
        parentTransform = transf;
        targetPos = target;
        anim1 = a1;
        anim2 = a2;
        canMove = true;
    }

    private void StopMovement()
    {
        canMove = false;
        transform.position = targetPos;
        transform.SetParent(parentTransform);
        //Plays animations
        anim1.Play();
        anim2.Play();
        FindObjectOfType<ScoreManager>().IncrementScore(transform.childCount);      //Increments score
        parentTransform.gameObject.GetComponent<Circle>().isActive = true;
        parentTransform.gameObject.GetComponent<Circle>().CheckFullSlices();
        transform.localScale = new Vector3(1f, 1f, 1f);
        FindObjectOfType<MainCircle>().Spawn();     //Spawns a new slice
    }
}
